﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Drawing;
using System.Globalization;
using System.Collections;

namespace kata
{
    class Program
    {
        static void Main(string[] args)
        {


            string[] b = new string[] { "NORTH", "WEST", "SOUTH", "EAST" }; ;
            var x = dirReduc(b);
           Console.WriteLine(x[0]);
            Console.ReadLine();
            
          
        }
        public static Func<int, int> GetFunction(int[] sequence)
        {

            int patron = 0;
            for (int i = 0; i < sequence.Length - 1; i++)
            {
                int patron1 = sequence[i + 1] - sequence[i];
                if (patron == 0)
                {
                    patron = patron1;
                }
                else if (patron1 != patron)
                {
                    throw new ArgumentException();
                }

            }
            List<int> s = sequence.ToList();
            for (int i = 4; i < 1000; i++)
            {
                s.Add(s.ElementAt(i) + patron);
            }


            Func<int, int> myFunction = (int a) => s.ElementAt(a);

            return myFunction;
        }
        public static bool isPalindrome(int w, out int numero)
        {
            Regex s = new Regex("[^a-zA-Z0-9]");

            string numString = s.Replace(w.ToString(), "");
            int k = numString.Length - 1;
            string newString = "";
            for (int i = 0; k >= 0; i++)
            {
                newString += numString[k];
                k--;
            }
            if (numString.ToLower().Equals(newString.ToLower()))
            {
                numero = 0;
                return true;
            }
            else
            {
                numero = int.Parse(newString);
                return false; 
             }
        }
        public static string[] FiftyShadesOfGray(long n)
        {
            List<string> countOfGrey = new List<string>();
            for (int i = 1; i <= 1; i++)
            {
                Color myColor = Color.FromArgb(1, 1, 1);

                string hex = "#" + myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");
                countOfGrey.Add(hex);

            }
            return countOfGrey.ToArray();
        }
        public static int[] UpArray(int[] num)
        {
            if (num.Count(x => x<0 || x>9)>0)
            {

                return null;
            }
            int sum = 1;
            for (int i = num.Length - 1; i >= 0; i--)
            {
                if (num[i] + sum == 10)
                {
                    if (i != 0)
                    {
                        num[i] = 0;
                        sum = 1;
                    }
                    else
                    {
                        num[i] += sum;
                    }
                }
                else
                {
                    num[i] += sum;
                    sum = 0; 
                }
            }
            string s =string.Join("", num) ;
            return s.Select(x => (int)Char.GetNumericValue(x)).ToArray();
        }
        public static string GoodVsEvil(string good, string evil)
        {
            var goodArray = good.Split(' ').Select(x => Convert.ToInt32(x)).ToArray() ;
            var evilArray = evil.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
            List<int> goodValues = new List<int>() 
            {
               1,2,3,3,4,10
            };
            List<int> evilValues = new List<int>() 
            {
               1,2,2,2,3,5,10
            };

            int sumGoods=0;
            int sumEvils = 0;
            for (int i = 0; i < goodArray.Length; i++)
            {
                sumGoods += goodArray[i] * goodValues.ElementAt(i); 
            }
            for (int i = 0; i < evilArray.Length; i++)
            {
                sumEvils += evilArray[i] * evilValues.ElementAt(i);
            }
            if (sumGoods> sumEvils)
            {
                return "Battle Result: Good triumphs over Evil";
            }
            else if (sumEvils> sumGoods)
	         {
                 return "Battle Result: Evil eradicates all trace of Good";
	          }
            return "Battle Result: No victor on this battle field";

        }
        public static int Triangular(int n)
        {
            if (n < 0)
                return 0;
            int result = (n * (n + 1)) / 2;
            return result;
        }
        public static bool CheckCoupon(string enteredCode, string correctCode, string currentDate, string expirationDate)
        {
            var cDate = DateTime.Parse(currentDate, CultureInfo.InvariantCulture);
            var eDate = DateTime.Parse(expirationDate, CultureInfo.InvariantCulture);
            if (cDate.Date > eDate.Date || !enteredCode.Equals(currentDate))
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        public static String factors(int lst)
        {
            if (lst<0)
            {
                return "";
            }
           int [] listFactors = ESieve(int.MaxValue);
           List<KeyValuePair<int,int>> lista = new List<KeyValuePair<int,int>>();
           int contador = 0;
           int number = 2;
           while (lst > 1)
           {
               for (int i = 0; i < listFactors.Length; i++)
               {
                   if (lst % listFactors[i] == 0)
                   {

                       lst /= listFactors[i];
                       if ( number != listFactors[i])
                       {
                           if(contador>0)
                            lista.Add(new KeyValuePair<int,int>(number,contador));
                           number = listFactors[i];
                           contador = 0;
                       }
                      
                       contador++;
                       break;
                   }
                   if (contador ==0 && i==listFactors.Length-1)
                   {
                      lista.Add(new KeyValuePair<int, int>(number, contador));
                       number = lst;
                       lst /= lst;
                     
                       contador++;
                   }
                   else if (contador > 0 &&  i == listFactors.Length - 1)
                   {
                       lista.Add(new KeyValuePair<int, int>(number, contador));
                       number = lst;
                       lst /= lst;
                       contador = 1;
                       
                   }
               }
           }
           lista.Add(new KeyValuePair<int, int>(number, contador));

           string result ="";
           foreach (var i in lista)
           {
               result += i.Value == 1 ? string.Format("({0})", i.Key) : string.Format("({0}**{1})",i.Key,i.Value);
           }
           return result;
        }


        static int[] ESieve(int upperLimit)
        {
            int sieveBound = (int)(upperLimit - 1) / 2;
            int upperSqrt = ((int)Math.Sqrt(upperLimit) - 1) / 2;

            BitArray PrimeBits = new BitArray(sieveBound + 1, true);

            for (int i = 1; i <= upperSqrt; i++)
            {
                if (PrimeBits.Get(i))
                {
                    for (int j = i * 2 * (i + 1); j <= sieveBound; j += 2 * i + 1)
                    {
                        PrimeBits.Set(j, false);
                    }
                }
            }

            List<int> numbers = new List<int>((int)(upperLimit / (Math.Log(upperLimit) - 1.08366)));
            numbers.Add(2);

            for (int i = 1; i <= sieveBound; i++)
            {
                if (PrimeBits.Get(i))
                {
                    numbers.Add(2 * i + 1);
                }
            }

            return numbers.ToArray();
        }
       
        public static string factors2(int lst)
        {
            string res = "";
            for (int factor = 2; factor <= lst; ++factor)
            {
                int cnt;
                for (cnt = 0; lst % factor == 0; ++cnt)
                {
                    lst /= factor;
                }
                if (cnt > 0)
                {
                    res += "(" + factor + (cnt > 1 ? "**" + cnt : "") + ")";
                }
            }
            return res;
        }
        public static int GetMissingElement(int[] superImportantArray)
        {
           return  superImportantArray.Sum(x => x)-45;
        }
        public static string[] dirReduc(String[] arr)
        {
            List<String> list = arr.ToList();
            for (int i = 0; i <= list.Count -2; i++)
            {
                if (isOposide(list.ElementAt(i), list.ElementAt(i+1)))
                {

                    list.RemoveAt(i);
                    list.RemoveAt(i);
                    i = -1;
                }
            }
           
            return list.ToArray();
        }
        public static bool isOposide(string a , string b)
        {
            if ((a.Equals("NORTH") && b.Equals("SOUTH")) ||  (b.Equals("NORTH") && a.Equals("SOUTH")) )
            {
                return true;
            }
            else if ((a.Equals("EAST") && b.Equals("WEST")) || (b.Equals("EAST") && a.Equals("WEST")))
            {
                return true;
            }
            return false;
        }
    }
}
